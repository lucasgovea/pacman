package br.com.lgovea.pacman;


import br.com.lgovea.lib.jplay.Animation;

/**
 *
 * @author Lucas
 */
public class Vida extends Animation {

	public Vida(String filename, int x, int y) {
		super(filename);
		this.setPosition(x, y);

	}

	public void desenha() {
		this.draw();
	}

}
