package br.com.lgovea.pacman;

import br.com.lgovea.lib.jplay.Sprite;
import br.com.lgovea.lib.jplay.Point;

/**
 *
 * @author Lucas
 */
public class Fantasma extends Sprite {

	private int escolha = (int) (Math.random() * 4);
	private int FantasmaDelay;
	private double velo = 0.05;

	public Fantasma(String filename, int x, int y) {
		super(filename);
		this.FantasmaDelay = 0;
		this.setPosition(x, y);
	}

	public void Desenha() {
		this.draw();
	}

	public void autoMover() {

		FantasmaDelay++;

		Point posicao = this.getPosition();

		switch (escolha) {

		case 0:
			this.moveToDown();
			this.setVelocityY(velo);
			;
			for (int i = 0; i < Labirinto.MAX_PAREDE; i++) {
				if (this.collided(Labirinto.paredes[i])) {
					this.setPosition(posicao);
				}
			}
			break;

		case 1:
			this.moveToRight();
			this.setVelocityX(velo);
			for (int i = 0; i < Labirinto.MAX_PAREDE; i++) {
				if (this.collided(Labirinto.paredes[i])) {
					this.setPosition(posicao);
				}
			}
			break;

		case 2:
			this.moveToUp();
			this.setVelocityY(velo);
			for (int i = 0; i < Labirinto.MAX_PAREDE; i++) {
				if (this.collided(Labirinto.paredes[i])) {
					this.setPosition(posicao);
				}
			}
			break;

		case 3:
			this.moveToLeft();
			this.setVelocityX(velo);
			for (int i = 0; i < Labirinto.MAX_PAREDE; i++) {
				if (this.collided(Labirinto.paredes[i])) {
					this.setPosition(posicao);
				}
			}
			break;

		}
		if (this.FantasmaDelay % 300 == 0) {
			escolha = (int) (Math.random() * 4);
		}

	}
}
