package br.com.lgovea.pacman;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.lgovea.lib.jplay.Sprite;

/**
 *
 * @author Lucas
 */
public class Comida extends Sprite {
	final static int MAX_COMIDA = 192;
	final static int MAX_POR_LINHA = 20;
	final static int MAX_POR_COLUNA = 26;
	
	List<Integer> posicoesLinhasX;
	List<Integer> posicoesColunasY;
	Map<Sprite, Boolean> comidas = new HashMap<>();
	
	public Comida(String filename) {
		super(filename);
		iniciarLinhasPosicoesX();
		iniciarColunasPosicoesY();
		
		for (int i = 0; i <= MAX_POR_LINHA; i++) { // Primeira Linha
			Sprite comida = new Sprite(filename);
			comida.setPosition(posicoesLinhasX.get(i), 27);
			comidas.put(comida, true);
		}
		for (int i = 1; i < MAX_POR_LINHA; i++) { // Segunda Linha
			Sprite comida = new Sprite(filename);
			comida.setPosition(posicoesLinhasX.get(i), 93);
			comidas.put(comida, true);
		}
		for (int i = 1; i < MAX_POR_LINHA; i++) { // Terceira Linha
			Sprite comida = new Sprite(filename);
			comida.setPosition(posicoesLinhasX.get(i), 161);
			comidas.put(comida, true);
		}
		for (int i = 1; i < MAX_POR_LINHA; i++) { // Quarta Linha
			Sprite comida = new Sprite(filename);
			comida.setPosition(posicoesLinhasX.get(i), 228);
			comidas.put(comida, true);
		}
		for (int i = 0; i <= MAX_POR_LINHA; i++) { // Quinta Linha
			Sprite comida = new Sprite(filename);
			comida.setPosition(posicoesLinhasX.get(i), 293);
			comidas.put(comida, true);
		}
		
		
		
		for (int i = 0; i < MAX_POR_COLUNA; i++) { // Primeira Coluna
			Sprite comida = new Sprite(filename);
			comida.setPosition(7, posicoesColunasY.get(i));
			comidas.put(comida, true);
		}
		for (int i = 0; i <= MAX_POR_COLUNA; i++) { // Segunda Coluna
			if(i != 0 && i != MAX_POR_COLUNA) {
				Sprite comida = new Sprite(filename);
				comida.setPosition(73, posicoesColunasY.get(i));
				comidas.put(comida, true);
			}
		}
		for (int i = 0; i <= MAX_POR_COLUNA; i++) { // Quarta Coluna
			if(i != 0 && i != MAX_POR_COLUNA) {
			Sprite comida = new Sprite(filename);
			comida.setPosition(141, posicoesColunasY.get(i));
			comidas.put(comida, true);
			}
		}
		for (int i = 0; i <= MAX_POR_COLUNA; i++) { // Quinta Coluna
			if(i != 0 && i != MAX_POR_COLUNA) {
			Sprite comida = new Sprite(filename);
			comida.setPosition(209, posicoesColunasY.get(i));
			comidas.put(comida, true);
			}
		}

	}

	public void comer(Sprite Object) {
		for (Map.Entry<Sprite, Boolean> entry : comidas.entrySet()) {
			if(Object.collided(entry.getKey()) && entry.getValue()) {
				entry.getKey().hide();
				entry.setValue(false);
				PacMan.pontuacao += 10;
			}
		}
	}

	public void Desenha() {
		for (Map.Entry<Sprite, Boolean> entry : comidas.entrySet()) {
			entry.getKey().draw();
		}
		
	}

	public boolean terminaJogo() {
		for (Map.Entry<Sprite, Boolean> entry : comidas.entrySet()) {
			if(entry.getValue()) {
				return false;
			}
		}
		return true;
	}
	
	private void iniciarLinhasPosicoesX() {
		posicoesLinhasX = new ArrayList<>();
		int inicio = 8;
		for (int i = 0; i <= MAX_POR_LINHA; i++) {
			posicoesLinhasX.add(inicio);
			inicio += 10;
		}
	}
	
	private void iniciarColunasPosicoesY() {
		posicoesColunasY = new ArrayList<>();
		int inicio = 27;
		for (int i = 0; i <= MAX_POR_COLUNA; i++) {
			posicoesColunasY.add(inicio);
			inicio += 10;
		}
	}

}
