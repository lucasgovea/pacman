package br.com.lgovea.pacman;

import br.com.lgovea.lib.jplay.GameObject;
import br.com.lgovea.lib.jplay.Keyboard;
import br.com.lgovea.lib.jplay.Point;
import br.com.lgovea.lib.jplay.Sprite;

/**
 *
 * @author Lucas
 */
public class PacMan extends Sprite {
	public static int pontuacao = 0;
	private Point posicao = getPosition();
	private final static double VELOCIDADE_MAX = 0.05;
	
	private static PacMan singleInstacePacMan;

	private PacMan() {
		super(Imagem.FRAME_FULL_PACMAN, 12);
		setPosition(3, 22);
	}
	
	public static PacMan getInstance() {
		if(singleInstacePacMan == null) {
			singleInstacePacMan = new PacMan();
		}
		return singleInstacePacMan;
	}

	public void ToSet() {
		setTimeChangeFrame(100);
		draw();
		setVelocityX(VELOCIDADE_MAX);
		setVelocityY(VELOCIDADE_MAX);
		runAnimation();
	}

	public void mover() {
		posicao = getPosition();
		moveX();
		moveY();

	}

	public void colisao(GameObject objeto) {
		if (this.collided(objeto) == true) {
			setPosition(posicao);
		}
	}

	public void MudaImagem() {

		if (keyboard.keyDown(Keyboard.LEFT_KEY)) {
			setRangeOfFrames(3, 5);
		} else if (keyboard.keyDown(Keyboard.RIGHT_KEY)) {
			setRangeOfFrames(0, 2);
		} else if (keyboard.keyDown(Keyboard.UP_KEY)) {
			setRangeOfFrames(9, 11);
		} else if (keyboard.keyDown(Keyboard.DOWN_KEY)) {
			setRangeOfFrames(6, 8);
		}

	}

}
