package br.com.lgovea.pacman;

import br.com.lgovea.lib.jplay.GameImage;
import br.com.lgovea.lib.jplay.Keyboard;
import br.com.lgovea.lib.jplay.Window;
import java.awt.Color;

/**
 *
 * @author Lucas
 */
public class Main {
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		Window janela = new Window(223, 325);
		GameImage win = new GameImage(Imagem.WIN);
		Keyboard teclado = janela.getKeyboard();
		GameImage over = new GameImage(Imagem.GAME_OVER);

		Labirinto labirinto = new Labirinto();

		Fantasma fantVermelho = new Fantasma(Imagem.FANT_VERMELHO, 69, 88);
		Fantasma fantRosa = new Fantasma(Imagem.FANT_ROSA, 137, 88);
		Fantasma fantAzul = new Fantasma(Imagem.FANT_AZUL, 69, 222);
		Fantasma fantLaranja = new Fantasma(Imagem.FANT_LARANJA, 137, 222);

		PacMan pac = PacMan.getInstance();

		Comida comida = new Comida(Imagem.COMIDA);

		Vida vida1 = new Vida(Imagem.PACMAN_STATIC, 3, 308);
		Vida vida2 = new Vida(Imagem.PACMAN_STATIC, 20, 308);
		Vida vida3 = new Vida(Imagem.PACMAN_STATIC, 37, 308);

		int quant = 0;
		boolean executando = true;

		while (executando) {

			labirinto.Desenha();

			comida.Desenha();
			comida.comer(pac);
			if (comida.terminaJogo()) {
				win.setPosition(50, 150);
				win.draw();
				pac.setVelocityX(0);
				pac.setVelocityY(0);
				teclado.removeKey(Keyboard.UP_KEY);
				teclado.removeKey(Keyboard.DOWN_KEY);
				teclado.removeKey(Keyboard.RIGHT_KEY);
				teclado.removeKey(Keyboard.LEFT_KEY);
				fantVermelho.setVelocityX(0);
				fantRosa.setVelocityX(0);
				fantAzul.setVelocityX(0);
				fantLaranja.setVelocityX(0);
				fantVermelho.setVelocityY(0);
				fantRosa.setVelocityY(0);
				fantAzul.setVelocityY(0);
				fantLaranja.setVelocityY(0);

				if (teclado.keyDown(Keyboard.ESCAPE_KEY) == true) {
					executando = false;
				}
			}

			fantVermelho.Desenha();
			fantRosa.Desenha();
			fantAzul.Desenha();
			fantLaranja.Desenha();
			fantVermelho.autoMover();
			fantRosa.autoMover();
			fantAzul.autoMover();
			fantLaranja.autoMover();

			pac.ToSet();
			pac.mover();
			pac.MudaImagem();

			for (int j = 0; j < Labirinto.MAX_PAREDE; j++) {
				pac.colisao(Labirinto.paredes[j]);
			}

			if (pac.collided(fantVermelho) || pac.collided(fantRosa) || pac.collided(fantAzul) || pac.collided(fantLaranja)) {
				janela.delay(1000);
				if (quant < 3) {
					quant++;
					pac.setPosition(3, 22);
					fantVermelho.setPosition(69, 88);
					fantRosa.setPosition(137, 88);
					fantAzul.setPosition(69, 222);
					fantLaranja.setPosition(137, 222);
					if (quant == 1) {
						vida3.hide();
					} else if (quant == 2) {
						vida2.hide();
					} else if (quant == 3) {
						vida1.hide();
					}

				} else {
					over.setPosition(30, 140);
					over.draw();
					pac.setVelocityX(0);
					pac.setVelocityY(0);
					teclado.removeKey(Keyboard.UP_KEY);
					teclado.removeKey(Keyboard.DOWN_KEY);
					teclado.removeKey(Keyboard.RIGHT_KEY);
					teclado.removeKey(Keyboard.LEFT_KEY);
					fantVermelho.setVelocityX(0);
					fantRosa.setVelocityX(0);
					fantAzul.setVelocityX(0);
					fantLaranja.setVelocityX(0);
					fantVermelho.setVelocityY(0);
					fantRosa.setVelocityY(0);
					fantAzul.setVelocityY(0);
					fantLaranja.setVelocityY(0);

					if (teclado.keyDown(Keyboard.ESCAPE_KEY)) {
						executando = false;
					}
				}
			}

			vida1.desenha();
			vida2.desenha();
			vida3.desenha();

			janela.drawText("Pontos: " + PacMan.pontuacao + "                        Esc para Sair", 5, 15, Color.white);

			janela.display();
			if (teclado.keyDown(Keyboard.ESCAPE_KEY)) {
				executando = false;
			}
		}

		janela.exit();

	}
}
