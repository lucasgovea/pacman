/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lgovea.pacman;

import br.com.lgovea.lib.jplay.GameImage;



/**
 *
 * @author Lucas
 */
public class Labirinto
{

   private GameImage fundo = new GameImage(Imagem.FUNDO);

   public static GameImage paredes[]= new GameImage[16];

   final static int MAX_PAREDE = 16;

   public void Desenha() {
      //Desenha fundo
      fundo.draw();

      
      
      paredes[0]= new GameImage(Imagem.LINHA_HORIZONTAL);
      paredes[0].setPosition(0, 20);

      paredes[1]=new GameImage(Imagem.LINHA_HORIZONTAL);
      paredes[1].setPosition(0, 303);

      paredes[2]=new GameImage(Imagem.LINHA_VERTICAL);
      paredes[2].setPosition(0, 20);

      paredes[3]=new GameImage(Imagem.LINHA_VERTICAL);
      paredes[3].setPosition(220, 20);

      paredes[4]=new GameImage(Imagem.QUADRADO);
      paredes[4].setPosition(17, 36);

      paredes[5]=new GameImage(Imagem.QUADRADO);
      paredes[5].setPosition(84, 36);

      paredes[6]=new GameImage(Imagem.QUADRADO);
      paredes[6].setPosition(152, 36);

      paredes[7]=new GameImage(Imagem.QUADRADO);
      paredes[7].setPosition(17, 103);

      paredes[8]=new GameImage(Imagem.QUADRADO);
      paredes[8].setPosition(84, 103);

      paredes[9]=new GameImage(Imagem.QUADRADO);
      paredes[9].setPosition(152, 103);

      paredes[10]=new GameImage(Imagem.QUADRADO);
      paredes[10].setPosition(17, 170);

      paredes[11]=new GameImage(Imagem.QUADRADO);
      paredes[11].setPosition(84, 170);

      paredes[12]=new GameImage(Imagem.QUADRADO);
      paredes[12].setPosition(152, 170);

      paredes[13]=new GameImage(Imagem.QUADRADO);
      paredes[13].setPosition(17, 237);

      paredes[14]=new GameImage(Imagem.QUADRADO);
      paredes[14].setPosition(84, 237);

      paredes[15]=new GameImage(Imagem.QUADRADO);
      paredes[15].setPosition(152, 237);

      for(int i=0; i<MAX_PAREDE; i++)
         paredes[i].draw();

   }


}
