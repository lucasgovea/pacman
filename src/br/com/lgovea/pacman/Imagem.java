package br.com.lgovea.pacman;

public interface Imagem {
	String GAME_OVER = "resource/gameover.png";
	String WIN = "resource/win.png";
	String FANT_VERMELHO = "resource/fant1.png";
	String FANT_ROSA = "resource/fant2.png";
	String FANT_AZUL = "resource/fant3.png";
	String FANT_LARANJA = "resource/fant4.png";
	String FUNDO = "resource/fundo.png";
	String QUADRADO = "resource/quad.png";
	String LINHA_HORIZONTAL = "resource/linhahor.png";
	String LINHA_VERTICAL = "resource/linhavert.png";
	String FRAME_FULL_PACMAN = "resource/framefullpac.png";
	String PACMAN_STATIC = "resource/pac_d_2.png";
	String COMIDA = "resource/p1.png";
}
